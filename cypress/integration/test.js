const { genData } = require("../fixtures/data")
const { inForgotPassword } = require("../support/LoginPage/forgotPasswordElements")
const { inLoginPage } = require("../support/LoginPage/loginPageElements")
const { inMainLobby } = require("../support/MainPage/mainLobbyElements")
const { inSignUp } = require("../support/LoginPage/signUpElements")

let userData
describe('experiments', () => {

    before(() => {
        cy.visit("/")
        cy.viewport('iphone-7')
    })

    it('verify API', () => {
        
        inLoginPage.usernameInput().type('arthur')
        inLoginPage.passwordInput().type('poker123')
        inLoginPage.loginButton().click()
        cy.url().should('contain', '/login')
        // const account = { "username": "arthur", "skinId": 50257, "password": "poker123" }
        // cy.request("POST", "https://stagingapi.pokerplaza.com/api/club/authenticate", account)
        //     .its('body').then(body => {
        //         console.log(body)
        //         expect(body.code).to.equal(200)
        //         // expect(body.status).to.equal(200)
        //     })
        // cy.visit('/profile')
        cy.url().should('include', '/home')
        inMainLobby.profile().click()
    })

    it('experiment 2', () => {
     
        cy.selectLanguage('Arabic')
        cy.navigateTo('/forgotPassword')
    })
    
    it.only('check ', () => {
        
        cy.intercept('GET', 'https://stagingapi.pokerplaza.com/lobby/allGames?').as('allGames')
        cy.loginUI('arthur', 'poker123')
        cy.wait('@allGames').its('response').then((availableProducts) => {
                
            console.log(availableProducts)
        })
    })

    it('Login test with new registered player', () => {
        
        while(genData.generateUsername()){
            
            let username = genData.generateUsername()
            
            if(username.length <= 16) {
                cy.registerViaApi(username, 'poker123', '50257')
                cy.intercept('POST', Cypress.env('authenticate')).as('loginrequest')
                cy.loginUI(username, 'poker123')
                cy.wait('@loginrequest').its('response.statusCode').should('equal', 200)
                cy.log('New player with username = '+ username + ' \created and logged in properly')
                break
            }
        
            else {
                cy.registerUI(username, 'poker123')
                inSignUp.usernameFieldFailed().should('exist')
                inSignUp.usernameError().should('contain', 'Max symbols 16')
            }

        }
    })
})