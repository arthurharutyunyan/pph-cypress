import { inLoginBonusPage } from '../../support/MainPage/loginBonus'
import { inMainLobbyWithoutClubs } from '../../support/MainPage/mainLobbyElementsWithoutClubs'
import accounts from '../../fixtures/accounts.json'
import { createClub, createClubFlow } from '../../support/MainPage/createClubFlowElements'

describe('Main page elements validation tests', () => {

    beforeEach( () => {
        
        cy.visit('/')
        cy.viewport('iphone-7')

    })

    it('Validates LoginBonus page', () => {

        let amount = 500
        
        cy.loginUI('arthur', 'poker123')
        
        // mocking askForBonus API response to always have login bonus screen
        cy.log('Mocking the API response to get login bonus')
        cy.intercept('POST', Cypress.env('askForBonus'), (req) => {
            req.reply(res => {
                res.body.result.message = "Bonus is granted!"
                res.body.result.amount = amount
            })
        }).as('askBonus')
        cy.wait('@askBonus').its('response.statusCode').should('equal', 200)
        cy.get('modal').invoke('show')
        cy.log('Verifying login bonus text')
        inLoginBonusPage.loginBonusText().should('have.text', 'Login Bonus')
        cy.log('Verifying coin image in login bonus screen')
        inLoginBonusPage.coinImage().should('have.css', 'background-image', 'url("https://pph.draft10.com/static/media/coin.a9412b42.svg")')
        cy.log('Verifying if the bonus amount is equal to API response amount')
        inLoginBonusPage.bonusAmount().should('have.text', amount).and('have.css', 'background-color', 'rgb(26, 77, 119)')
        cy.log('Verifying the poker cards in login bonus page')
        inLoginBonusPage.pokerCards().should('exist').and('have.css', 'background-image', 'url("https://pph.draft10.com/static/media/dailyBonusCards.67bc4e19.png")')
        cy.log('Verifying the Claim button visibility and the text')
        inLoginBonusPage.claimButton().should('be.visible').and('have.text', 'Claim')
        inLoginBonusPage.claimButton().click()
    })
    it('Validates main page elements without clubs', () => {

        cy.log('Loggin in and taking API response data')
        cy.intercept('POST', Cypress.env('authenticate')).as('login')
        cy.loginUI(accounts.withoutClub, accounts.password)
        cy.wait('@login').its('response').then(loginResponse => {

            expect(loginResponse.statusCode).to.equal(200)
            cy.log('Verifying if the balance in mainLobby equals the balance from the API')
            let balance = loginResponse.body.result.authenticationData[0].wallets[0].balance
            inMainLobbyWithoutClubs.leagueBalance().should('have.text', balance) 
        })
        
        cy.log('Verifying if the text CLUB is there in the main lobby')
        inMainLobbyWithoutClubs.clubText().should('have.text', 'Clubs')
        cy.log('verifying the Message button existance and visibility')
        inMainLobbyWithoutClubs.messageButton().should('be.visible')
        cy.log('Verifying coin image inside the balance widget')
        inMainLobbyWithoutClubs.coinIcon()
            .should('be.visible')
            .should('have.css', 'background-image', 'url("https://pph.draft10.com/static/media/coin.a9412b42.svg")')
        
        cy.log('Verifying "Join Club" button existance and its text')
        inMainLobbyWithoutClubs.joinClubWithoutClubs().should('be.visible').and('have.text', 'Join Club')
        cy.log('Verifying "Create Club" button existance and its text')
        inMainLobbyWithoutClubs.createClubWithoutClubs().should('be.visible').and('have.text', 'Create Club')
        
        cy.log('Verifying Home and Profile buttons on the footer')
        inMainLobbyWithoutClubs.home().should('have.class', 'footerItem home active')
            .invoke('attr', 'aria-current')
            .should('equal', 'page')
        inMainLobbyWithoutClubs.profile().click()
        inMainLobbyWithoutClubs.profile()
            .should('have.class', 'active')
            .invoke('attr', 'aria-current')
            .should('equal', 'page')
        inMainLobbyWithoutClubs.home()
            .should('not.have.class', 'footerItem home active')
            .should('not.have.attr', 'aria-current')
        inMainLobbyWithoutClubs.home().click()

        cy.log('Verifying League Games text and its wrapper functionality')
        inMainLobbyWithoutClubs.leagueGamesText().should('have.text', 'League Games')
        inMainLobbyWithoutClubs.leagueWrapperButton()

        cy.get('.leagueWrapper ').invoke('attr', 'class').should('include', 'opened')
        inMainLobbyWithoutClubs.leagueWrapperButton().click()
        cy.get('.leagueWrapper ').invoke('attr', 'class').should('include', 'closed')
        inMainLobbyWithoutClubs.leagueWrapperButton().click()
        
    })

    it('Validates main page elements with club/s', () => {
        
    })

    it('Validates the global skin(LeagueGames) available products', () => {

        // all possible products
        let products = {
            appProducts: ["Sport", "Casino", "Live Casino", "Poker"],
            apiProducts: ["betting", "casino", "liveCasino", "poker"]
        }

        cy.log('Loging in to grab the APIs data from authenticate and allGames')
        cy.intercept('POST', Cypress.env('authenticate')).as('login')
        cy.intercept('GET', Cypress.env('allGames')).as('allGames')
        cy.loginUI(accounts.withClub, accounts.password)
        cy.wait('@login').its('response.statusCode').should('equal', 200)
        cy.wait('@allGames').its('response').then((availableProducts) => {
                
            console.log(availableProducts)
            expect(availableProducts.statusCode).to.equal(200)
                let keys = Object.keys(availableProducts.body)
                // removeing unnecessary "code" from the array, it is the last element
                keys.pop()

                for (let i = 0; i < products.apiProducts.length; i++) {
                    if (keys.includes(products.apiProducts[i])) {
                        cy.log('The ' + products.appProducts[i] + ' should be visible')
                        cy.get('.leagueContent').contains(products.appProducts[i]).should('be.visible')
                            .and('have.text', products.appProducts[i])
                    }
                    else{ cy.log('The ' + products.appProducts[i] + ' is not available for this club')}
                    
                }
            })

                
    })

    it('Validates Join-Club flow', () => {

    })
})