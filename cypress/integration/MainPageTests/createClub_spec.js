import { inMainLobbyWithoutClubs } from '../../support/MainPage/mainLobbyElementsWithoutClubs'
import accounts from '../../fixtures/accounts.json'
import { createClub, createClubFlow } from '../../support/MainPage/createClubFlowElements'

describe('Verifying club creation flow', () => {

    beforeEach(() => {

        cy.visit('/')
        cy.viewport('iphone-7')
    })

    it('Validates the elements in Create-Club dialog', () => {

        cy.log('Logging in with an account without club')
        cy.loginUI(accounts.withoutClub, accounts.password)

        cy.log('clicking to "createClub" button')
        inMainLobbyWithoutClubs.createClubWithoutClubs().click()

        cy.log('checking elements visibility in createClub modal')
        createClubFlow.createDialogItself().should('be.visible')
        createClubFlow.createDialogTitle().should('contain', "Create a Club")

        cy.log('chekcing close button functionality')
        createClubFlow.createDialogClose().should('be.visible').click()
        createClubFlow.createDialogItself().should('not.exist')

        cy.log('checking input field vs button')
        inMainLobbyWithoutClubs.createClubWithoutClubs().click()
        createClubFlow.createDialoginputFieldLabel().should('contain', 'Enter code for the club creation')
        createClubFlow.createDialogInput().should('be.visible')
            .invoke('attr', 'type')
            .should('include', 'number')
        createClubFlow.createDialogCreateButton()
            .should('contain', 'Create a club')
            .should('have.attr', 'disabled')

        createClubFlow.createDialogInput().type('213')
        createClubFlow.createDialogCreateButton().should('not.have.attr', 'disabled')

        createClubFlow.createDialogInput().clear()
        createClubFlow.createDialogCreateButton().should('have.attr', 'disabled')
        createClubFlow.createDialogInput().type('123')
        createClubFlow.createDialogCreateButton().click()

    })

    it('Validates the elements in "Submit" modal', () => {

        cy.log('Logging in with an account without club')
        cy.loginUI(accounts.withoutClub, accounts.password)
        inMainLobbyWithoutClubs.createClubWithoutClubs().click()

        createClubFlow.createDialogInput().type('213')
        createClubFlow.createDialogCreateButton().click()

        cy.log('checking the Submit modal elements')
        createClubFlow.sumbitDialogModal().should('be.visible')
        createClubFlow.submitDialogHeader().should('be.visible').and('contain', 'Create Club')

        cy.log('checking close button in Submit modal')
        createClubFlow.submitDialogCloseButton().should('be.visible').click()
        createClubFlow.sumbitDialogModal().should('not.exist')

        inMainLobbyWithoutClubs.createClubWithoutClubs().click()
        createClubFlow.createDialogInput().type('213')
        createClubFlow.createDialogCreateButton().click()

        createClubFlow.submitDialogAvatar().should('be.visible')
            .invoke('attr', 'type')
            .should('include', 'file')
        createClubFlow.submitDialogClubNameLabel().should('be.visible').and('contain', 'Club Name')
        createClubFlow.submitDialogClubNameInput().should('be.visible')
            .invoke('attr', 'type')
            .should('include', 'text')
        createClubFlow.submitDialogDescriptionLabel().should('be.visible').and('contain', 'Club Description and Rules')
        createClubFlow.submitDialogDescriptionInput().should('be.visible')
        createClubFlow.submitDialogSubmitButton().should('be.visible').and('contain', 'Submit')
    })

    it.only('Hierarchy code generation', () => {

        let hierarchyCode
        cy.log('Loging in to BusinessCenter via API to get the account token')
        let loginCred = { "username": "clubtopmasterskin", "password": "poker123", "loginFromBC": 1, "captchaResponse": "" }
        cy.request('POST', Cypress.env('bcAuthenticate'), loginCred).then(response => {

            console.log(response)
            let token = response.body.result.token
            console.log(token)
            
            cy.log('Posting the hierarchy code to DB via API')
            cy.request('POST', Cypress.env('generateHierarchyCode'), { "token": token }).then(postCode => {

                console.log(postCode)
                expect(postCode.body.code).to.equal(201)
                expect(postCode.body.result.message).to.contain('Successfully created')
            })
            
            cy.log('Get the hierarchy code from the API respons')
            cy.request('GET', Cypress.env('generateHierarchyCode'), { "token": token }).then(code => {

                expect(code.body.result.clubCodeData[0].status).to.equal('new')

                if (code.body.result.clubCodeData[0].status === 'new') {

                    hierarchyCode = code.body.result.clubCodeData[0].code
                }

                cy.log('Loging in to ClubApp via app to create a club')
                cy.loginUI(accounts.withoutClub, accounts.password)

                cy.intercept('POST', Cypress.env('clubDataApi')).as('clubData')
                inMainLobbyWithoutClubs.createClubWithoutClubs().click()
                createClubFlow.createDialogInput().type(hierarchyCode)
                createClubFlow.createDialogCreateButton().click()

                createClubFlow.submitDialogClubNameInput().type('club1')
                createClubFlow.submitDialogDescriptionInput().type('This is the club for only Cypressers!!')
                createClubFlow.submitDialogSubmitButton().click()
                cy.wait('@clubData').its('response').then(clubData => {
                    
                    cy.log('Vslidating the club data received from the API')
                    expect(clubData.body.code).to.be.equal(200)
                    expect(clubData.body.result.authenticationData[0].nickname).to.contain('club1')
                    expect(clubData.body.result.authenticationData[0].typeId).to.be.equal(3)
                    expect(clubData.body.result.authenticationData[0].username).to.contain('club1')
                    expect(clubData.body.result.authenticationData[0].wallets[0].balance).to.be.equal(0)
                    expect(clubData.body.result.authenticationData[0].wallets[0].currency).to.be.contain('TRY')
                    expect(clubData.body.result.authenticationData[0].wallets[1].balance).to.be.equal(0)
                    expect(clubData.body.result.authenticationData[0].wallets[1].currency).to.be.contain('FPP')

                })
            })
        })

    })
})