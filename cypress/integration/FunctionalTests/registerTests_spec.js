const { inSignUp } = require("../../support/LoginPage/signUpElements")
const { translateSignUp } = require("../../fixtures/translations/signUp.json")
const { genData } = require("../../fixtures/data")

describe('Registration Tests', () => {

    beforeEach(() => {

        cy.visit('/register')
        cy.viewport('iphone-7')
    })

    it('Check Username field functionality', () => {

        // check the <3 symbol case
        cy.log('checking username.length < 3 case')
        inSignUp.usernameInput().invoke('attr', 'placeholder').should('include', translateSignUp.english.username)
        inSignUp.usernameInput().type('aa')
        inSignUp.usernameFieldFailed().should('be.visible')
        inSignUp.usernameError()
            .should('be.visible')
            .and('contain', translateSignUp.english.usernameMustBe)
            .and('have.css', 'color', 'rgb(255, 90, 90)')

        // check >= 3 positive case
        cy.log('checking username.length >=3 case')
        inSignUp.usernameInput().type('a')
        inSignUp.usernameSuccess().should('be.visible')
        inSignUp.usernameError().should('not.be.visible')
        inSignUp.usernameFieldFailed().should('not.exist')
        inSignUp.usernameInput().clear()

        //check username length >16 case
        cy.log('checking username.length > 16 case')
        inSignUp.usernameInput().type('morethansixteencharacters1')
        inSignUp.usernameFieldFailed().should('be.visible')
        inSignUp.usernameError()
            .should('be.visible')
            .and('contain', translateSignUp.english.usernameIsTooLong)
            .and('have.css', 'color', 'rgb(255, 90, 90)')
        inSignUp.usernameInput().clear()

        //check empty case
        cy.log('checking empty username case')
        inSignUp.passwordInput().type('poker123')
        inSignUp.registerButton().click()
        inSignUp.usernameFieldFailed().should('be.visible')
        inSignUp.usernameError()
            .should('be.visible')
            .and('contain', translateSignUp.english.enterUsername)
            .and('have.css', 'color', 'rgb(255, 90, 90)')

        // check forbidden symbols case
        cy.log('checking forbidden symbols case')
        let forbiddenSymbols = ['.', '-', '>', '<', '?', "/", "|", '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '=', '[', ']', '{', '}']
        for (let i in forbiddenSymbols) {

            inSignUp.usernameInput().clear().type('aa' + forbiddenSymbols[i])
            inSignUp.usernameFieldFailed().should('be.visible')
            inSignUp.usernameError()
                .should('be.visible')
                .and('contain', translateSignUp.english.usernameShouldNotContain)
                .and('have.css', 'color', 'rgb(255, 90, 90)')
        }
        inSignUp.usernameInput().clear()
    })

    it('Check Password field functionality', () => {

        cy.log('check the password field placeholder')
        inSignUp.passwordInput()
            .invoke('attr', 'placeholder')
            .should('include', translateSignUp.english.password)

        cy.log('check password field type')
        inSignUp.passwordInput()
            .invoke('attr', 'type')
            .should('include', 'password')

        cy.log('password field type after password eye click')
        inSignUp.passwordEye().click()
        inSignUp.passwordInput()
            .invoke('attr', 'type')
            .should('include', 'text')
        inSignUp.passwordEye().click()
    })

    it('Check password field errors', () => {

        cy.log('check password with only letters')
        inSignUp.passwordInput().type('poker')
        inSignUp.passwordError()
            .should('be.visible')
            .and('contain', translateSignUp.english.passwordMustBe)
            .and('have.css', 'color', 'rgb(255, 90, 90)')
        inSignUp.passwordFieldFailed().should('be.visible')

        cy.log('check password with letter+digits but <6')
        inSignUp.passwordInput().clear().type('po1')
        inSignUp.passwordError()
            .should('be.visible')
            .and('contain', translateSignUp.english.passwordMustBe)
            .and('have.css', 'color', 'rgb(255, 90, 90)')

        inSignUp.passwordFieldFailed().should('be.visible')

        cy.log('check password with more than 16')
        inSignUp.passwordInput().clear().type('morethansixteensymbols3')
        inSignUp.passwordError()
            .should('be.visible')
            .and('contain', translateSignUp.english.passwordMustBe)
            .and('have.css', 'color', 'rgb(255, 90, 90)')

        inSignUp.passwordFieldFailed().should('be.visible')

        cy.log('check empty password error')
        inSignUp.passwordInput().clear()
        inSignUp.registerButton().click()
        inSignUp.passwordError()
            .should('be.visible')
            .and('contain', translateSignUp.english.enterPassword)
            .and('have.css', 'color', 'rgb(255, 90, 90)')

        inSignUp.passwordFieldFailed().should('be.visible')

        cy.log('check correct password')
        inSignUp.passwordInput().clear().type('poker123')
        inSignUp.passwordError().should('not.be.visible')
        inSignUp.passwordSuccess().should('be.visible')
    })
    
    it('Register with already existing username', () => {
        
        let username = genData.generateUsername()
        let password = "poker123"
        cy.registerViaApi(username, password, '500005')

        cy.registerUI(username, password)
        inSignUp.usernameError()
            .should('be.visible')
            .and('contain', translateSignUp.english.usernameIsTaken)
            .and('have.css', 'color', 'rgb(255, 90, 90)')
        
        inSignUp.usernameInput().clear()
        inSignUp.passwordInput().clear()
    })
    it('Check agreement checkbox functionality', () => {

        inSignUp.termsAndConditions().find('label')
            .should('contain', 'I have read')
            .and('contain', 'and')
        
        inSignUp.termsAndConditions().find('label').find('a[class="tertiary"]').eq(0)
            .should('contain', 'Terms and Conditions')
            .should('have.attr', 'href')

        inSignUp.termsAndConditions().find('label').find('a[class="tertiary"]').eq(1)
            .should('contain', 'Privacy Policy')
            .should('have.attr', 'href')

        inSignUp.privacyPolicyCheckbox().check({force:true})
        cy.get('span[class="mask false"]').find('svg').should('exist')

        inSignUp.privacyPolicyCheckbox().uncheck({force:true})
        cy.get('span[class="mask false"]').find('svg').should('not.exist')

        inSignUp.registerButton().click()
        inSignUp.privacyError()
            .should('exist')
            .should('be.visible')
            .should('contain', translateSignUp.english.pleaseAcceptPrivacy)
            .should('have.css', 'color', 'rgb(255, 90, 90)')

    })

    it('Correct register from UI', () => {

        cy.intercept('POST', Cypress.env('registerApi')).as('registerRequest')
        cy.registerUI(genData.generateUsername(), 'poker123')
        cy.wait('@registerRequest').its('response.statusCode').should('equal', 200)

        cy.wait(2000)
        cy.contains('Claim').click()
        cy.logout()
    })
})