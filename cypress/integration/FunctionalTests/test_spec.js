describe('First test', () => {

    beforeEach( () => {
        cy.visit('/')
        cy.viewport('iphone-7')
    })

    it('test the login', () => {

        cy.get('.userNameInput').type('arthur')
        cy.get('.passwordField').find('input').type('poker123')
        cy.contains('Login').click()
    })
})
