const { genData } = require("../../fixtures/data")
const { inLoginPage } = require("../../support/LoginPage/loginPageElements")
const { translateLoginPage } = require("../../fixtures/translations/login.json")

describe('Login Tests', () => {

    beforeEach( () => {
        cy.visit('/')
        cy.viewport('iphone-7')
    })

    it('Login test with EMPTY fields', () => {

        inLoginPage.loginButton().click()
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.english.fieldIsEmpty)
        inLoginPage.usernameFailedField().should('exist').and('be.visible')
        inLoginPage.passwordFieldError().should('be.visible').and('contain', translateLoginPage.english.fieldIsEmpty)
        inLoginPage.passwordFailedField().should('exist').and('be.visible')
    })

    it('Login test with correct USername and BLANK password', () => {

        inLoginPage.usernameInput().type('arthur')
        inLoginPage.loginButton().click()

        inLoginPage.passwordFieldError().should('be.visible').and('contain', translateLoginPage.english.fieldIsEmpty)
        inLoginPage.passwordFailedField().should('exist').and('be.visible')

        inLoginPage.usernameInput().clear()
    })

    it('Login test with correct Password and BLANK username', () => {

        inLoginPage.passwordInput().type('poker123')
        inLoginPage.loginButton().click()

        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.english.fieldIsEmpty)
        inLoginPage.usernameFailedField().should('exist').and('be.visible')

        inLoginPage.passwordInput().clear()
    })

    it('Login test with correct Username and incorrect password', () => {
        
        cy.intercept('POST', Cypress.env('authenticate')).as('loginrequest')
        inLoginPage.usernameInput().type('arthur')
        inLoginPage.passwordInput().type('dfkjdh1')
        inLoginPage.loginButton().click()
        cy.wait('@loginrequest').its('response.statusCode').should('equal', 403)

        inLoginPage.passwordFieldError().should('be.visible').and('contain', translateLoginPage.english.passwordIncorrect)

        inLoginPage.usernameInput().clear()
        inLoginPage.passwordInput().clear()
    })

    it('Login test with correct Password and incorrect username', () => {

        cy.intercept('POST', Cypress.env('authenticate')).as('loginrequest')
        inLoginPage.usernameInput().type('ghghghghg')
        inLoginPage.passwordInput().type('poker123')
        inLoginPage.loginButton().click()
        cy.wait('@loginrequest').its('response.statusCode').should('equal', 404)

        inLoginPage.usernameFailedField().should('exist').and('be.visible')
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.english.usernameIncorrect)

        inLoginPage.usernameInput().clear()
        inLoginPage.passwordInput().clear()
    })

    it('Verify password EYE functionality', () => {

        inLoginPage.passwordInput()
            .invoke('attr', 'type')
            .should('include', 'password')
        
        inLoginPage.passwordEye().click()
        inLoginPage.passwordInput()
            .invoke('attr', 'type')
            .should('include', 'text')

        inLoginPage.passwordEye().click()
        inLoginPage.passwordInput()
            .invoke('attr', 'type')
            .should('include', 'password')
    })

    it('Login test with correct credentials', () => {
        
        cy.intercept('POST', Cypress.env('authenticate')).as('loginrequest')
        cy.loginUI('arthur', 'poker123')
        cy.wait('@loginrequest').its('response.statusCode').should('equal', 200)

        cy.logout()
    })

    it('Login test with new registered player', () => {
        
        let username = genData.generateUsername()
        cy.log('Going to register a new player with username = '+ username)
        cy.registerViaApi(username, 'poker123', '500005')
        cy.intercept('POST', Cypress.env('authenticate')).as('loginrequest')
        cy.loginUI(username, 'poker123')
        cy.wait('@loginrequest').its('response.statusCode').should('equal', 200)
        cy.log('New player with username = '+ username + ' created and logged in properly')
    })
})