const { inLoginPage } = require("../../support/LoginPage/loginPageElements")
const translateLoginPage = require("../../fixtures/translations/login.json")

describe('Login page Localization tests elements', () => {

    beforeEach(() => {
        cy.visit("/login")
        cy.viewport('iphone-7')
    })
    
    it('Localization for English', () => {

        inLoginPage.signIn().should('contain', translateLoginPage.english.signIn)
        inLoginPage.usernameText().should('contain', translateLoginPage.english.username)
        inLoginPage.usernameInput()
            .invoke('attr', 'placeholder')
            .should('include', translateLoginPage.english.username)
        inLoginPage.passwordText().should('contain', translateLoginPage.english.password)
        inLoginPage.passwordInput()
            .invoke('attr', 'placeholder')
            .should('include', translateLoginPage.english.password)
        inLoginPage.forgotPassword().should('contain', translateLoginPage.english.forgotPassword)
        inLoginPage.loginButton().should('contain', translateLoginPage.english.loginButton)
        inLoginPage.dontHaveAccount().should('contain', translateLoginPage.english.dontHaveAccount)
        inLoginPage.registerNow().should('contain', translateLoginPage.english.registerNow)
        inLoginPage.termsConditions().should('contain', translateLoginPage.english.termsConditions)

        inLoginPage.loginButton().click()
        inLoginPage.passwordFieldError().should('be.visible').and('contain', translateLoginPage.english.fieldIsEmpty)
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.english.fieldIsEmpty)
        
        
        inLoginPage.usernameInput().type('arthur')
        inLoginPage.passwordInput().type('aaaaaaaa')
        inLoginPage.loginButton().click()
        inLoginPage.passwordFieldError().should('contain', translateLoginPage.english.passwordIncorrect)

        inLoginPage.passwordInput().clear().type('poker123')
        inLoginPage.usernameInput().type('aaaaaa')
        inLoginPage.loginButton().click()
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.english.usernameIncorrect)


    })

    it('Localization for Arabic', () => {

        cy.selectLanguage('Arabic')

        // inLoginPage.signIn().should('contain', '')  - uncomment this once the arabic translation is added
        inLoginPage.usernameText().should('contain', translateLoginPage.arabic.username)
        inLoginPage.usernameInput()
            .invoke('attr', 'placeholder')
            .should('include', translateLoginPage.arabic.username)
        inLoginPage.passwordText().should('contain', translateLoginPage.arabic.password)
        inLoginPage.passwordInput()
            .invoke('attr', 'placeholder')
            .should('include', translateLoginPage.arabic.password)
        inLoginPage.forgotPassword().should('contain', translateLoginPage.arabic.forgotpassword)
        inLoginPage.loginButton().should('contain', translateLoginPage.arabic.loginButton)
        inLoginPage.dontHaveAccount().should('contain', translateLoginPage.arabic.dontHaveAccount)
        inLoginPage.registerNow().should('contain', translateLoginPage.arabic.registerNow)
        inLoginPage.termsConditions().should('contain', translateLoginPage.arabic.termsConditions)

        inLoginPage.loginButton().click()
        inLoginPage.passwordFieldError().should('be.visible').and('contain', translateLoginPage.arabic.fieldIsEmpty)
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.arabic.fieldIsEmpty)
        
        
        inLoginPage.usernameInput().type('arthur')
        inLoginPage.passwordInput().type('aaaaaaaa')
        inLoginPage.loginButton().click()
        inLoginPage.passwordFieldError().should('contain', translateLoginPage.arabic.passwordIncorrect)

        inLoginPage.passwordInput().clear().type('poker123')
        inLoginPage.usernameInput().type('aaaaaa')
        inLoginPage.loginButton().click()
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.arabic.usernameIncorrect)

        
    })

    it('Localization for French', () => {

        cy.selectLanguage('French')

        // inLoginPage.signIn().should('contain', '')  - uncomment this once the french translation is added
        inLoginPage.usernameText().should('contain', translateLoginPage.french.username)
        inLoginPage.usernameInput()
            .invoke('attr', 'placeholder')
            .should('include', translateLoginPage.french.username)
        inLoginPage.passwordText().should('contain', translateLoginPage.french.password)
        inLoginPage.passwordInput()
            .invoke('attr', 'placeholder')
            .should('include', translateLoginPage.french.password)
        inLoginPage.forgotPassword().should('contain', translateLoginPage.french.forgotpassword)
        inLoginPage.loginButton().should('contain', translateLoginPage.french.loginButton)
        inLoginPage.dontHaveAccount().should('contain', translateLoginPage.french.dontHaveAccount)
        inLoginPage.registerNow().should('contain', translateLoginPage.french.registerNow)
        inLoginPage.termsConditions().should('contain', translateLoginPage.french.termsConditions)

        inLoginPage.loginButton().click()
        inLoginPage.passwordFieldError().should('be.visible').and('contain', translateLoginPage.french.fieldIsEmpty)
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.french.fieldIsEmpty)
        

        inLoginPage.usernameInput().type('arthur')
        inLoginPage.passwordInput().type('aaaaaaaa')
        inLoginPage.loginButton().click()
        inLoginPage.passwordFieldError().should('be.visible').and('contain', translateLoginPage.french.passwordIncorrect)

        inLoginPage.passwordInput().clear().type('poker123')
        inLoginPage.usernameInput().type('aaaaaa')
        inLoginPage.loginButton().click()
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.french.usernameIncorrect)


    })

    it('Localization for Portuguese', () => {

        cy.selectLanguage('Portuguese')

        inLoginPage.signIn().should('contain', translateLoginPage.portuguese.signIn)
        inLoginPage.usernameText().should('contain', translateLoginPage.portuguese.username)
        inLoginPage.usernameInput()
            .invoke('attr', 'placeholder')
            .should('include', translateLoginPage.portuguese.username)
        inLoginPage.passwordText().should('contain', translateLoginPage.portuguese.password)
        inLoginPage.passwordInput()
            .invoke('attr', 'placeholder')
            .should('include', translateLoginPage.portuguese.password)
        inLoginPage.forgotPassword().should('contain', translateLoginPage.portuguese.forgotpassword)
        inLoginPage.loginButton().should('contain', translateLoginPage.portuguese.loginButton)
        inLoginPage.dontHaveAccount().should('contain', translateLoginPage.portuguese.dontHaveAccount)
        inLoginPage.registerNow().should('contain', translateLoginPage.portuguese.registerNow)
        inLoginPage.termsConditions().should('contain', translateLoginPage.portuguese.termsConditions)

        inLoginPage.loginButton().click()
        inLoginPage.passwordFieldError().should('be.visible').and('contain', translateLoginPage.portuguese.fieldIsEmpty)
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.portuguese.fieldIsEmpty)
        
        inLoginPage.passwordInput().clear()
        inLoginPage.usernameInput().type('arthur')
        inLoginPage.passwordInput().type('aaaaaaaa')
        inLoginPage.loginButton().click()
        inLoginPage.passwordFieldError().should('be.visible').and('contain', translateLoginPage.portuguese.passwordIncorrect)

        inLoginPage.passwordInput().clear().type('poker123')
        inLoginPage.usernameInput().clear().type('aaaaaa')
        inLoginPage.loginButton().click()
        inLoginPage.usernameFieldError().should('be.visible').and('contain', translateLoginPage.portuguese.usernameIncorrect)

    })
})