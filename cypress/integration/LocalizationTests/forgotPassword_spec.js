const { inForgotPassword } = require("../../support/LoginPage/forgotPasswordElements")
const { inLoginPage } = require("../../support/LoginPage/loginPageElements")
const { translateForgotPassword } = require ("../../fixtures/translations/forgotPassword.json")

describe('Forgot password page Localization tests', () => {

    beforeEach(() => {
        cy.visit('/')
        cy.viewport('iphone-7')
    })
// CHECK FOR THE API MAKE SURE IT IS IMPLEMENTED PROPERLY
    
    it('Localization of ForgotPassword English', () => {

        inLoginPage.forgotPassword().click()

        inForgotPassword.recoveryTitle().should('contain', translateForgotPassword.english.recoveryTitle)
        inForgotPassword.recoveryDescription().should('contain', translateForgotPassword.english.recoveryDescription)
        inForgotPassword.emailText().should('contain', translateForgotPassword.english.emailText)
        inForgotPassword.emailInput()
            .invoke('attr', 'placeholder')
            .should('include', translateForgotPassword.english.emailPlaceholder)
        inForgotPassword.sendEmailButton().should('contain', translateForgotPassword.english.sendEmailButton)
        inForgotPassword.emailInput().type('aaaa')
        inForgotPassword.errorMessage().should('contain', translateForgotPassword.english.wrongEmailError)

        inForgotPassword.emailInput().clear().type('aaa23@fake.com')
        inForgotPassword.sendEmailButton().click()
        inForgotPassword.errorMessage().should('contain', translateForgotPassword.english.emailIsNotConnected)

        inForgotPassword.back().click()

    })

    it.skip('Verifies email input with /not connected/ email address', () => {

        inLoginPage.forgotPassword().click()
        inForgotPassword.emailInput().type('aaa')
        inForgotPassword.errorMessage()
            .should('exist')
            .and('contain', 'Please provide a valid email')
        inForgotPassword.failedField().should('exist').and('be.visible')

        inForgotPassword.emailInput().clear().should('be.empty')
        inForgotPassword.emailInput().type('aaaa@fake.com')
        inForgotPassword.errorMessage().should('not.be.visible')
        inForgotPassword.failedField().should('not.be.visible')
        inForgotPassword.successField().should('exist').and('be.visible')
        inForgotPassword.sendEmailButton().click()
        inForgotPassword.emailNotConnected()
            .should('exist')
            .and('be.visible')
            .should('contain', 'This Email not connected to any account.')
        inForgotPassword.failedField()
            .should('exist')
            .and('be.visible')

        inForgotPassword.back().click()


    })

    it('Localization of ForgotPassword Arabic', () => {

        cy.selectLanguage('Arabic')
        inLoginPage.forgotPassword().click()

        inForgotPassword.recoveryTitle().should('contain', translateForgotPassword.arabic.recoveryTitle)
        inForgotPassword.recoveryDescription().should('contain', translateForgotPassword.arabic.recoveryDescription)
        inForgotPassword.emailText().should('contain', translateForgotPassword.arabic.emailText)
        inForgotPassword.emailInput()
            .invoke('attr', 'placeholder')
            .should('include', translateForgotPassword.arabic.emailPlaceholder)
        inForgotPassword.sendEmailButton().should('contain', translateForgotPassword.arabic.sendEmailButton)
        inForgotPassword.emailInput().type('aaaa')
        inForgotPassword.errorMessage().should('contain', translateForgotPassword.arabic.wrongEmailError)

        inForgotPassword.emailInput().clear().type('aaa23@fake.com')
        inForgotPassword.sendEmailButton().click()
        inForgotPassword.errorMessage().should('contain', translateForgotPassword.arabic.emailIsNotConnected)

        inForgotPassword.back().click()

    })

    it('Localization of ForgotPassword French', () => {

        cy.selectLanguage('French')
        inLoginPage.forgotPassword().click()

        inForgotPassword.recoveryTitle().should('contain', translateForgotPassword.french.recoveryTitle)
        inForgotPassword.recoveryDescription().should('contain', translateForgotPassword.french.recoveryDescription)
        inForgotPassword.emailText().should('contain', translateForgotPassword.french.emailText)
        inForgotPassword.emailInput()
            .invoke('attr', 'placeholder')
            .should('include', translateForgotPassword.french.emailPlaceholder)
        inForgotPassword.sendEmailButton().should('contain', translateForgotPassword.french.sendEmailButton)

        inForgotPassword.emailInput().type('aaa')
        inForgotPassword.errorMessage().should('contain', translateForgotPassword.french.wrongEmailError)
        
        inForgotPassword.emailInput().clear().type('aaa@fake.com')
        inForgotPassword.sendEmailButton().click()
        inForgotPassword.errorMessage().should('contain', translateForgotPassword.french.emailIsNotConnected)
        
            inForgotPassword.back().click()
    })

    it('Localization of ForgotPassword Portuguese', () => {

        cy.selectLanguage('Portuguese')
        inLoginPage.forgotPassword().click()

        inForgotPassword.recoveryTitle().should('contain', translateForgotPassword.portuguese.recoveryTitle)
        inForgotPassword.recoveryDescription().should('contain', translateForgotPassword.portuguese.recoveryDescription)
        inForgotPassword.emailText().should('contain', translateForgotPassword.portuguese.emailText)
        inForgotPassword.emailInput()
            .invoke('attr', 'placeholder')
            .should('include', translateForgotPassword.portuguese.emailPlaceholder)
        inForgotPassword.sendEmailButton().should('contain', translateForgotPassword.portuguese.sendEmailButton)

        inForgotPassword.emailInput().type('aaa')
        inForgotPassword.errorMessage().should('contain', translateForgotPassword.portuguese.wrongEmailError)
        
        inForgotPassword.emailInput().clear().type('aaa@fake.com')
        inForgotPassword.sendEmailButton().click()
        inForgotPassword.errorMessage().should('contain', translateForgotPassword.portuguese.emailIsNotConnected)
        
        inForgotPassword.back().click()
        

    })

})