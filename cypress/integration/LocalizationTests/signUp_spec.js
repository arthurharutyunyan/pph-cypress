const { inSignUp } = require("../../support/LoginPage/signUpElements")
const { translateSignUp } = require("../../fixtures/translations/signUp.json")

describe('SignUp page Localization tests', () => {
    
    beforeEach( () => {
        cy.visit('/register')
        cy.viewport('iphone-7')
    })

    it('Localization for SignUp English', () => {

        inSignUp.signUp().should('contain', 'Sign Up')
        inSignUp.usernameText().should('contain', "Username")
        inSignUp.usernameInput().invoke('attr', 'placeholder').should('include', 'Username')
        inSignUp.passwordText().should('contain', 'Password')
        inSignUp.passwordInput().invoke('attr', 'placeholder').should('include', 'Password')
        inSignUp.termsAndConditions()
            .should('contain', 'I have read ')
            .and('contain', 'Terms and Conditions')
            .and('contain', 'and')
            .and('contain', 'Privacy Policy')
        inSignUp.alreadyHaveAccount().should('contain', 'Already have an account? ').and('contain', 'Sign in')

        inSignUp.usernameInput().type('2')
        inSignUp.usernameError().should('be.visible').and('contain', 'The username must be at least 3 symbols')
        inSignUp.usernameInput().clear()

        inSignUp.passwordInput().type('aa12#')
        inSignUp.passwordError().should('be.visible').and('contain', 'The password must contain both letters and digits with 6 - 16 characters')
        inSignUp.passwordInput().clear()
        
        inSignUp.registerButton().click()
        inSignUp.usernameError().should('be.visible').and('contain', 'Please, enter a username')
        inSignUp.passwordError().should('be.visible').and('contain', 'Please, enter a password')
        inSignUp.privacyError().should('be.visible').and('contain', 'Please Accept terms and conditions')
        // cy.screenshot('Signup_english')


    })

    it('Localization of SignUp Arabic', () => { 

        cy.selectLanguage('Arabic')
        // enable when trnaslation is added
        // inSignUp.signUp().should('contain', translateSignUp.arabic.signup)
        inSignUp.usernameText().should('contain', translateSignUp.arabic.username)
        inSignUp.usernameInput().invoke('attr', 'placeholder').should('include', translateSignUp.arabic.username)
        inSignUp.passwordText().should('contain', translateSignUp.arabic.password)
        inSignUp.passwordInput().invoke('attr', 'placeholder').should('include', translateSignUp.arabic.password)
        // enable when trnaslation is added
        // inSignUp.registerButton().should('contain', translateSignUp.arabic.signup)
        // inSignUp.termsAndConditions()
        //     .should('be.visible')
            // .and('contain', translateSignUp.arabic.IhaveRead)
            // .and('contain', translateSignUp.arabic.termsAndConditions)
        //     .and('contain', 'and')
            // .and('contain', translateSignUp.arabic.privacyPolicy)
        inSignUp.alreadyHaveAccount()
            .should('contain', translateSignUp.arabic.alreadyHaveAccount)
            .and('contain', translateSignUp.arabic.signIn)

        inSignUp.usernameInput().type('2')
        inSignUp.usernameError().should('contain', translateSignUp.arabic.usernameMustBe)
        inSignUp.usernameInput().clear()

        inSignUp.passwordInput().type('aa12#')
        inSignUp.passwordError().should('contain', translateSignUp.arabic.passwordMustBe)
        inSignUp.passwordInput().clear()

        inSignUp.registerButton().click()
        inSignUp.usernameError().should('contain', translateSignUp.arabic.fieldIsEmpty)
        inSignUp.passwordError().should('contain', translateSignUp.arabic.fieldIsEmpty)
        inSignUp.privacyError().should('contain', translateSignUp.arabic.pleaseAcceptPrivacy)
    })

    it('Localization of SignUp French', () => {

        cy.selectLanguage('French')
        // enable when trnaslation is added
        // inSignUp.signUp().should('contain', translateSignUp.french.signup)
        inSignUp.usernameText().should('contain', translateSignUp.french.username)
        inSignUp.usernameInput().invoke('attr', 'placeholder').should('include', translateSignUp.french.username)
        inSignUp.passwordText().should('contain', translateSignUp.french.password)
        inSignUp.passwordInput().invoke('attr', 'placeholder').should('include', translateSignUp.french.password)
        // enable when trnaslation is added
        // inSignUp.registerButton().should('contain', translateSignUp.french.signup)
        // inSignUp.termsAndConditions()
        //     .should('be.visible')
            // .and('contain', translateSignUp.french.IhaveRead)
            // .and('contain', translateSignUp.french.termsAndConditions)
        //     .and('contain', 'and')
            // .and('contain', translateSignUp.french.privacyPolicy)
        inSignUp.alreadyHaveAccount()
            .should('contain', translateSignUp.french.alreadyHaveAccount)
            .and('contain', translateSignUp.french.signIn)

        inSignUp.usernameInput().type('2')
        inSignUp.usernameError().should('contain', translateSignUp.french.usernameMustBe)
        inSignUp.usernameInput().clear()

        inSignUp.passwordInput().type('aa12#')
        inSignUp.passwordError().should('contain', translateSignUp.french.passwordMustBe)
        inSignUp.passwordInput().clear()

        inSignUp.registerButton().click()
        inSignUp.usernameError().should('contain', translateSignUp.french.fieldIsEmpty)
        inSignUp.passwordError().should('contain', translateSignUp.french.fieldIsEmpty)
        inSignUp.privacyError().should('contain', translateSignUp.french.pleaseAcceptPrivacy)
    })

    it('Localization of SignUp Portuguese', () => {

        cy.selectLanguage('Portuguese')
        // enable when trnaslation is added
        // inSignUp.signUp().should('contain', translateSignUp.portuguese.signup)
        inSignUp.usernameText().should('contain', translateSignUp.portuguese.username)
        inSignUp.usernameInput().invoke('attr', 'placeholder').should('include', translateSignUp.portuguese.username)
        inSignUp.passwordText().should('contain', translateSignUp.portuguese.password)
        inSignUp.passwordInput().invoke('attr', 'placeholder').should('include', translateSignUp.portuguese.password)
        inSignUp.registerButton().should('contain', translateSignUp.portuguese.signup)
        inSignUp.termsAndConditions()
            .should('be.visible')
            .and('contain', translateSignUp.portuguese.IhaveRead)
            .and('contain', translateSignUp.portuguese.termsAndConditions)
            .and('contain', translateSignUp.portuguese.and)
            .and('contain', translateSignUp.portuguese.privacyPolicy)
        inSignUp.alreadyHaveAccount()
            .should('contain', translateSignUp.portuguese.alreadyHaveAccount)
            .and('contain', translateSignUp.portuguese.signIn)

        inSignUp.usernameInput().type('2')
        inSignUp.usernameError().should('contain', translateSignUp.portuguese.usernameMustBe)
        inSignUp.usernameInput().clear()

        inSignUp.passwordInput().type('aa12#')
        inSignUp.passwordError().should('contain', translateSignUp.portuguese.passwordMustBe)
        inSignUp.passwordInput().clear()

        inSignUp.registerButton().click()
        inSignUp.usernameError().should('contain', translateSignUp.portuguese.enterUsername)
        inSignUp.passwordError().should('contain', translateSignUp.portuguese.enterPassword)
        inSignUp.privacyError().should('contain', translateSignUp.portuguese.pleaseAcceptPrivacy)
    })
})  