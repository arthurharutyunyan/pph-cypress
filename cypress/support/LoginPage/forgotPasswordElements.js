export class forgotPassword {

    back() {

        return cy.get('.arrowIcon')
    }

    recoveryTitle() {

        return cy.get('.recoveryTitle')
    }

    recoveryDescription() {

        return cy.get('.recoveryDescription')
    }

    emailText() {

        return cy.get('.emailField')
    }

    emailInput() {

        return cy.get('.emailInput')
    }
    
    sendEmailButton() {

        return cy.get('.sendSection').find('.primary')
     }

     errorMessage() {

        return cy.get('span[class="errorMessage alignErr"]')
     }

     failedField() {

        return cy.get('.failedField')
     }

     successField() {

        return cy.get('.successField')
     }

}

export const inForgotPassword = new forgotPassword