export class loginPage {

    languageDropdown() {
        
        return cy.get('.css-ux50fn-singleValue')
    }

    signIn() {
        
        return cy.get('.title')
    }

    usernameText() {

        return cy.get('.userNameField')
    }

    usernameInput() {

        return cy.get('.userNameInput')
    }

    passwordText() {

        return cy.get('.passwordField')
    }

    passwordInput() {

        return cy.get('.passwordField').find('input')
    }

    passwordEye() {

        return cy.get('.password')
    }

    forgotPassword() {

        return cy.get('a[href="/forgotPassword"]')
    }

    loginButton() {

        return cy.get('.login').find('.primary')
    }

    dontHaveAccount() {

        return cy.get('p[class="registerLink register"]')
    }

    registerNow() {

        return cy.get('a[href="/register"]')
    }

    termsConditions() {

        return cy.get('.loginPageFooter').find('.tertiary')
    }

    usernameFieldError() {
        
        return cy.get('.userNameField').find('span[class="errorMessage alignErr"]')
    }

    usernameFailedField() {

        return cy.get('.userNameField').find('span[class="failedField"]')
     }

     passwordFieldError() {

        return cy.get('.passwordField').find('span[class="errorMessage alignErr"]')
     }

     passwordFailedField() {

        return cy.get('.passwordField').find('span[class="failedField"]')
     }

}

export const inLoginPage = new loginPage