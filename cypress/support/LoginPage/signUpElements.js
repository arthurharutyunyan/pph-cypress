export class signUpElements {

    signUp() {

        return cy.get('.title')
    }

    usernameText() {

        return cy.get('div[class="userNameField signUp"]')
    }

    usernameInput() {

        return cy.get('input[class="userNameInput"]')
    }

    passwordText() {

        return cy.get('div[class="passwordField signUp"]')
    }
    
    passwordInput(){

        return cy.get('.passwordField').find('input')

    }
    passwordField() {

        return cy.get('.passwordField')
    }

    usernameError() {

        return cy.get('div[class="userNameField signUp"]').find('span[class="errorMessage alignErr"]')
    }

    passwordError() {

        return cy.get('div[class="passwordField signUp"]').find('span[class="errorMessage alignErr"]')
    }

    registerButton() {

        return cy.get('.register').find('.primary')
    }

    usernameFieldFailed() {

        return cy.get('div[class="userNameField signUp"]').find('.failedField')
    }

    passwordFieldFailed() {

        return cy.get('div[class="passwordField signUp"]').find('.failedField')
    }

    usernameSuccess() {

        return cy.get('div[class="userNameField signUp"]').find('.successField')
    }

    passwordSuccess() {

        return cy.get('div[class="passwordField signUp"]').find('.successField')
    }

    passwordEye() {

        return cy.get('.password')
    }

    termsAndConditions() {

        return cy.get('.termsAndAgreement')
    }

    privacyError() {

        return cy.get('.termsError')
    }

    privacyPolicyCheckbox() {

        return cy.get('.userAgreement')
 
    }

    alreadyHaveAccount() {

        return cy.get('.registerLink')
    }
}

export const inSignUp = new signUpElements