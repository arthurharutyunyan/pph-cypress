Cypress.Commands.add('selectLanguage', (language) => {

    cy.get('.css-ux50fn-singleValue').click()
    cy.contains(language).click()
})

Cypress.Commands.add('loginUI', (username, password) => {

    cy.get('.userNameField').find('input').type(username)
    cy.get('.passwordField').find('input').type(password)
    cy.contains('Login').click()
})

Cypress.Commands.add('logout', () => {

    cy.contains('Profile').click()
    cy.contains('settings').click()
    cy.contains('Log Out').click()
})

Cypress.Commands.add('registerViaApi', (username, password, parentId) => {
    
    
    return cy.request('POST', Cypress.env('registerApi'), {"username": username, "password": password, "parentId": parentId})
        .its('body')
})

Cypress.Commands.add('registerUI', (username, password) => {

    cy.visit('/register')
    cy.get('.userNameInput').type(username)
    cy.get('input[type="password"]').type(password)
    cy.get('.userAgreement').check({force:true})
    cy.get('.register').find('.primary').click()
})
