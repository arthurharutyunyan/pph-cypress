export class linkEmail {

    linkEmailInput() {

        return cy.get('. emailContainer')
    }

    sendYourEmail() {

        return cy.get('.dialogConfirmBox')
    }
}

export const inLinkYourEmail = new linkEmail