export class loginOut {

    login(username, password) {

        cy.get('.userNameInput').type(username)
        cy.get('input[type="password"]').type(password)
        cy.get('.loginBtn').click()
    }
    
    logout() {

        cy.contains('Profile').click()
        cy.contains('span', 'settings').click()
        cy.contains('span', 'Log Out').click()
    }
    
    register(username) {

        cy.contains('Register Now!').click()
        cy.get('input[class="userNameInput"]').type(username)
        cy.get('input[type="password"]').type("poker123")
        cy.get('span[class="mask false"]').click()
        cy.contains('button', 'Sign Up').click()
        cy.get('.dialogConfirmBox > button').click()
    
    }

    joinToClub(clubId) {

        cy.contains('span', 'Join Club').click()
        cy.get('input[name="clubId"]').type(clubId)
        cy.get('div[class="dialogConfirmBox"]').click() 
    }

    // acceptJoinWithClub() {
    //     cy.get('p[class="icon members"]').click()
    //     cy.contains('New Members').click()
    //     for (let i)
    // }


}

export const justDo = new loginOut