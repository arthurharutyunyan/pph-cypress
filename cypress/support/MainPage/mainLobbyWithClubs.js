export class mainLobbyWithoutClubs {

    joinClub() {

        return cy.get('.clubTitle ').find('.secondary')
    }

    createClub() {

        return cy.get('.clubTitle').find('.primary')
    }

    
}

export const inMainLobbyWithoutClubs = new mainLobbyWithoutClubs