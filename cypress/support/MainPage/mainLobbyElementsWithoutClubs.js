export class mainLobbyWithoutClubs {

    profile() {

        return cy.get('a[href="/profile"]')
    }

    home() {

        return cy.get('a[href="/home"]')
    }

    clubText() {

        return cy.get('div[class="headerMainContainer"]').find('span[class="clubText"]')
    }

    coinIcon() {

        return cy.get('.headerMainContainer').find('span[class="coinIcon"]')
    }

    leagueBalance() {

        return cy.get('.headerMainContainer').find('span[class="balanceNumber"]')
    }

    messageButton() {

        return cy.get('.headerMainContainer').find('a[class="msgOpener newMessage"]')
    }

    joinClubWithoutClubs() {

        return cy.get('.search').find('.secondary')
    }

    createClubWithoutClubs() {

        return cy.get('.join').find('.primary')
    }

    leagueGamesText() {

        return cy.get('.leagueGames').find('span')
    }

    leagueWrapperButton() {

        return cy.get('.leagueGamesHeader').find('button[class="closeButton"]')
    }

    
}

export const inMainLobbyWithoutClubs = new mainLobbyWithoutClubs