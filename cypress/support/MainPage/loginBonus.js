export class loginBonus {

    loginBonusText() {

        return cy.get('.loginBonusText')
    }

    coinImage() {

        return cy.get('.coin').find('.coinImage')
    }

    bonusAmount() {

        return cy.get('.bonus').find('.coinAmount')
    }

    pokerCards() {

        return cy.get('.cards')
    }

    claimButton() {

        return cy.get('.dialogConfirmBox').find('.primary')
    }

}

export const inLoginBonusPage = new loginBonus;
