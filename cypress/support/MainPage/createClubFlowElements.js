export class createClub {

    createDialogItself() {

        return cy.get('modal')
    }

    createDialogTitle() {

        return cy.get('header[class="slim"]').find('span')
    }

    createDialogClose() {

        return cy.get('modal').find('.closeButton')
    }

    createDialoginputFieldLabel() {

        return cy.get('label[class="label form-label"]')
    }

    createDialogInput() {

        return cy.get('.form-control')
    }

    createDialogCreateButton() {

        return cy.get('.dialogConfirmBox').find('button')
    }

    sumbitDialogModal() {

        return cy.get('modal')
    }

    submitDialogHeader() {

        return cy.get('header').find('span')
    }

    submitDialogCloseButton(){

        return cy.get('header').find('.closeButton')
    }

    submitDialogAvatar() {

        return cy.get('#uploadAvatar')
    }

    submitDialogClubNameLabel() {

        return cy.get('div[class="clubName form-group"]').find('label[class="label form-label"]')
    }

    submitDialogClubNameInput() {

        return cy.get('.form-control')
    }

    submitDialogDescriptionLabel() {

        return cy.get('div[class="description form-group"]').find('label[class="label form-label"]')
    }

    submitDialogDescriptionInput() {

        return cy.get('div[class="description form-group"]').find('textarea')
    }

    submitDialogSubmitButton() {

        return cy.get('.dialogConfirmBox').find('button')
    }
}

export const createClubFlow = new createClub