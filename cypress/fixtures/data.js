const faker = require('faker')

export class userData {

    generateUsername() {

        let c
        let b
        let a = faker.internet.userName()
        let forbiddenSymbols = ['.', '-', '>', '<', '?', "/", "|", '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '+', '=', '[', ']', '{', '}']
        for (let i in forbiddenSymbols) {
            if (a.includes(forbiddenSymbols[i])) {
                c = a.replace(forbiddenSymbols[i], "_")
                b = c.slice(0, 16)
                return b;
            }
        }
        return a.slice(0, 16)
    }

}

export const genData = new userData
